"use strict";

class incompleteConfirmPopup extends registerPopup
{
    constructor(className) {
        super(className);
        $('.js-reg-incomplete-confirm-success').on('click', this.confirmSuccess.bind(this));
    }

    confirmSuccess(e) {
        e.preventDefault();
        this.fireEvent('success');
    }
}
