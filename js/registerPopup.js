"use strict";

class registerPopup
{
    constructor(className) {
        this.className = className;
        this.$el = $(className);
        this.$close = this.$el.find('.js-reg-popup-close');
        this.$fader = $('.fader');
        this.$collection = $('.js-reg-popup');
        this.onClose(this.sendSmsAndClose);
    }

    show() {
        this.$collection.hide();
        scrollTop(240, function(){
            this.$fader.show();
            this.$el.show();
        }.bind(this));
    }

    onClose(callback, data = null) {
        this.$close.off('click').on('click', data, callback.bind(this));
    }

    close() {
        this.$collection.hide();
        this.$fader.hide();
    }

    sendSmsAndClose(e) {
        $.ajax({
            url: '/user/register/sendsms',
            type: 'POST',
            data: e.data,
            dataType: 'json'
        })
        .fail(function() {
            console.error('sendsms has failed');
        })
        .always(function() {
            this.close();
            window.location.reload();
        }.bind(this));
    }

    on(event, callback) {
        this.$el.on(event, callback.bind(this));
        return this;
    }

    off(event) {
        this.$el.off(event);
        return this;
    }

    fireEvent(event, data) {
        this.$el.trigger(event, data);
    }
}
