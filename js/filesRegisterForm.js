"use strict";

class filesRegisterForm extends registerForm
{
    constructor(className) {
        super(className);
        this.dropzone = this.dropzone();
        this.dropzoneEvents();
    }

    dropzone() {
        return new Dropzone(this.className+" .drop-file", {
            url: this.$form.attr('action'),
            addRemoveLinks: true,
            uploadMultiple: true,
            autoProcessQueue: false,
            parallelUploads: 3,
            maxFiles: 2,
            maxFilesize: 2.0,
            acceptedFiles: '.jpg, .jpeg, .png, .bmp',
            dictMaxFilesExceeded: 'Максимальное число файлов {{maxFiles}}',
            dictFileTooBig: 'Максимальный размер загружаемого файла {{maxFilesize}}MB',
            dictInvalidFileType: 'Допустимые форматы файлов: .jpg .jpeg .png .bmp'
        });
    }

    dropzoneEvents() {
        this.dropzone.on('successmultiple', (files, data) => this.done(JSON.parse(data)) );
        this.dropzone.on('error', this.showValidationErrors);
        this.dropzone.on("addedfile", (file) => {
            this.dropzone.files.slice(this.dropzone.options.maxFiles).map(this.dropzone.removeFile.bind(this.dropzone));
            $('.drop-file__text').hide();
            let counter = document.createElement('div');
            counter.className = 'dz-count';
            counter.innerHTML = this.dropzone.files.length + '.';
            file.previewElement.insertBefore(counter, file.previewElement.firstChild);
        });
        this.dropzone.on("removedfile", () => {
            let errors = [];
            $('.drop-file__text').toggle(this.dropzone.files.length === 0);
            $('.dz-count').each(function(ind) {
                $(this).text(ind+1);
            });
            $(".dz-error-message").each(function() {
                let text = $(this).text();
                if (text != '') {
                    errors.push(text);
                }
            });
            errors = Array.from(new Set(errors)); // array.unique
            this.showValidationErrors(errors);
        });
    }

    showValidationErrors(errors) {
        let $message = $(".js-reg-validation-error");
        if (errors.length > 0) {
            $message.html(errors.join('<br>'));
        }
        $message.toggle(errors.length > 0);
    }

    submit(e) {
        e.preventDefault();
        let length = this.dropzone.files.length;
        if (length == 0) {
            return this.success();
        }
        if (length > 0 && length < 2) {
            return this.showValidationErrors(['Необходимо прикрепить два файла']);
        }
        if (this.dropzone.getRejectedFiles().length == 0) {
            return this.dropzone.processQueue();
        }
    }
}
