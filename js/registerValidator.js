"use strict";

class registerValidator
{
    constructor($form) {
        this.$form = $form;
        this.$fields = $form.find('.js-reg-validation-field');
        this.inputSelector = 'input';
        this.labelSelector = '.js-reg-validation-error';
    }

    /*
    *   server validation, outputs validation errors
    */
    showMessages(messages) {
        messages.forEach(function(message) {
            let $input = this.$fields.find(this.inputSelector).filter('[name="'+message.field+'"]');
            this.rejectField(this.$fields.has($input), message.text);
        }.bind(this));
        var $notRejected = this.$fields.filter(':not(.js-reg-validation-rejected)');
        $notRejected.each(function(index, field) {
            let $field = $(field);
            if(this.isEmpty($field)) {
                return;
            }
            this.approveField($field);
        }.bind(this));
    }

    clean() {
        this.$fields.find(this.labelSelector).text('').hide();
        this.$fields.find(this.inputSelector).removeClass('error');
        this.$fields.find(this.inputSelector).removeClass('valid');
        this.$fields.removeClass('js-reg-validation-rejected');
        this.$fields.removeClass('js-reg-validation-approved');
    }

    rejectField($field, messageText) {
        $field.addClass('js-reg-validation-rejected');
        $field.find(this.inputSelector).removeClass('valid').addClass('error');
        $field.find(this.labelSelector).text(messageText).show();     
    }

    approveField($field) {
        $field.addClass('js-reg-validation-approved');
        $field.find(this.inputSelector).removeClass('error').addClass('valid');
        $field.find(this.labelSelector).text('').hide();
    }

    isEmpty($field) {
        var $input = $field.find(this.inputSelector);
        var type = $input.attr('type');
        if(type == 'text') {
            return $input.val() == '';
        }
    }

    /*
    // js validation
    // e.g. 
    // <input data-validate="required" data-validate-error="Введите поле">
    // <input data-validate="email|empty" data-validate-error="Неверный email (если введён)">
    // <input data-validate="anything|empty">
    validate() {
        var success = true;
        this.$fields.find(this.inputSelector+'[data-validate]').each(function(index, input) {
            var $input = $(input);
            var $field = $input.closest('.js-reg-validation-field');
            var opts = $input.attr('data-validate').split('|');
            var validator = opts[0]+'Validator';
            if($.inArray('empty', opts) != -1 && this.isEmpty($field)) {
                //skip field if it is marked as allowed to be empty and it is empty indeed
                return; //continue;
            }
            if(this[validator]($input)) {
                this.approveField($field)
            } else {
                this.rejectField($field, $input.data('validate-error'));
                success = false;
            }
        }.bind(this));
        return success;
    };

    anythingValidator($input) {
        return true;
    };

    phoneValidator($input) {
        var patternLogin = new RegExp(/^(\+7|7|8)[\d]{10}$/);
        return patternLogin.test($input.val());
    };

    passwordValidator($input) {
        var patternPass = new RegExp(/.{8,20}$/);
        return patternPass.test($input.val());
    };

    emailValidator($input) {
        var patternEmail = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return patternEmail.test($input.val());
    };

    requiredValidator($input) {
        var type = $input.attr('type');
        if(type == 'checkbox') {
            return $input.is(':checked');
        }
        return $input.val() != '';
    };
    */
}
