"use strict";

class passportRegisterForm extends registerForm
{
    constructor(className) {
        super(className);
        $('.js-reg-passport-decline').on('click', () => this.fireEvent('decline'));
    }
}
