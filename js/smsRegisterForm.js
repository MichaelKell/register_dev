"use strict";

class smsRegisterForm extends registerForm
{
    constructor(className) {
        super(className);
        this.repeatTimeout = 60;
        this.$repeatLink = $('.js-reg-sms-repeat-link');
        this.$repeatCounter = $('.js-reg-sms-repeat-counter');
        this.$repeatCounterCount = $('.js-reg-sms-repeat-counter-count');
        this.$repeatLink.on('click', debounce(this.repeatOnSetTimeout.bind(this), this.repeatOnClearTimeout.bind(this), this.repeatTimeout*1000));    // click with timeout
    }

    validationError(data) {
        this.validator.showMessages(data.error.messages);
        if (data.repeatSms) {
            this.$repeatLink.show();
        }
    }

    success(data) {
        this.$repeatLink.hide();
        this.$repeatCounter.hide();
        this.fireEvent('success', data);
    }

    repeatOnSetTimeout() {
        this.$repeatLink.addClass('context-link__disabled');
        this.startCounter();
        this.send('/user/register/authphone').done(function (data) {
            if(data.status == 'error') {
                this.fireEvent('fail');
            }
        }.bind(this));
    }

    repeatOnClearTimeout() {
        this.$repeatLink.removeClass('context-link__disabled');
        this.$repeatCounter.hide();
        this.$repeatCounterCount.text('');
    }

    startCounter() {
        this.$repeatCounterCount.text(this.repeatTimeout);
        this.$repeatCounter.show();
        let seconds = this.repeatTimeout;
        let timer = setInterval(() => {
            seconds--;
            this.$repeatCounterCount.text(seconds);
            if (seconds <= 0) {
                this.$repeatCounter.hide();
                this.$repeatCounterCount.text('');
                clearInterval(timer);
            }
        }, 1000);
    }
}
