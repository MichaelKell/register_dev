"use strict";

class Register
{
    constructor() {
        this.quickForm = new quickRegisterForm('.js-reg-popup-quick');
        this.smsForm = new smsRegisterForm('.js-reg-popup-sms');
        this.passportForm = new passportRegisterForm('.js-reg-popup-passport');
        //this.fullForm = new fullRegisterForm('.js-reg-popup-full');
        this.filesForm = new filesRegisterForm('.js-reg-popup-files');
        this.incompleteConfirm = new incompleteConfirmPopup('.js-reg-popup-incomplete-confirm');
        this.incompleteNotice = new registerPopup('.js-reg-popup-incomplete');
        this.successNotice = new registerPopup('.js-reg-popup-success');
        this.failNotice = new registerPopup('.js-reg-popup-fail');
    }

    attachEvents() {
        $('.showReg').on('click', this.quickForm.show.bind(this.quickForm));

        this.quickForm.on('success', (e, data) => { this.gaQuickForm(); return data.cupis_code == 0 ? this.smsForm.show() : this.incompleteConfirm.show() });
        this.smsForm.on('success', (e, data) => { data.cupis_code == 0 ? this.passportForm.show() : this.incompleteConfirm.show() });
        this.passportForm.on('success', (e,data) => {
            if (data.confirmed == true) {
                this.successNotice.show()
            } else if (data.confirmed == false && data.attempts < 3) {
                this.passportForm.validator.rejectField(this.passportForm.validator.$fields.eq(0), 'Некорректные данные документа');
            } else {
                this.incompleteNotice.show();  
            }
        });
        //this.fullForm.on('success', () => this.filesForm.show());
        this.filesForm.on('success', () => this.incompleteNotice.show());
        this.incompleteConfirm.on('success', () => this.filesForm.show());

        this.passportForm.on('decline', () => this.incompleteNotice.show());

        this.quickForm.on('fail', this.fail.bind(this, 'quickForm'));
        this.smsForm.on('fail', this.fail.bind(this, 'smsForm'));
        this.passportForm.on('fail', this.fail.bind(this, 'passportForm'));
        //this.fullForm.on('fail', this.fail.bind(this, 'fullForm'));
        this.filesForm.on('fail', this.fail.bind(this, 'filesForm'));
    }

    fail(sender) {
        if (sender == 'quickForm') {
            this.failNotice.onClose(this.failNotice.close);
        } else {
            this.failNotice.onClose(this.failNotice.sendSmsAndClose);
        }
        this.failNotice.show();
    }

    gaQuickForm() {
        ga('send', 'event', 'Registration', 'Bingo');
        ga(function(tracker) { var clientId = tracker.get('clientId'); });
    }
}

var register = new Register();
register.attachEvents();
