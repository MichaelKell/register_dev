"use strict";

class fullRegisterForm extends registerForm
{
    constructor(className) {
        super(className);
        this.docTypeChange();
    }

    docTypeChange() {
        $('.js-reg-doctype-input').on('change', function() {
            let $docNumberLabel = $('.js-reg-docnumber-label');
            let $docSeriesLabel = $('.js-reg-docseries-label');
            let docNumberText = $docNumberLabel.text();
            let docSeriesText = $docSeriesLabel.text();
            if ($(this).val() == 1) {
                $docNumberLabel.text(docNumberText+"*");
                $docSeriesLabel.text(docSeriesText+"*");
            } else if(docNumberText.slice(-1) == "*") {
                $docNumberLabel.text(docNumberText.substr(0, docNumberText.length-1));
                $docSeriesLabel.text(docSeriesText.substr(0, docSeriesText.length-1));
            }
        });
    }
}
