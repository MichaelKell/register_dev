"use strict";

class registerForm extends registerPopup
{
    constructor(className) {
        super(className);
        this.$form = this.$el.find('form');
        this.$form.on('submit', this.submit.bind(this));
        this.validator = new registerValidator(this.$form);
    }

    send(url) {
       return $.ajax({
            url: url,
            type: 'POST',
            data: this.$form.serialize(),
            dataType: 'json',
            async: false
        });
    }

    submit(e) {
        e.preventDefault();
        this.validator.clean();
        this.send(this.$form.attr('action'))
        .done(this.done.bind(this))
        .fail(this.fail.bind(this));
    }

    done(data) {
        if (data.status == 'error') {
            if(data.error.type == 'validation') {
                this.validationError(data);
            }
            if(data.error.type == 'fatal') {
                this.fatalError(data);
            }
            return;
        }
        if (data.status == 'success') {
            this.success(data);
        }
    }

    fail(ajaxError) {
        console.error(ajaxError);
        this.fireEvent('fail');
    }
    
    success(data) {
        this.fireEvent('success', data);
    }

    validationError(data) {
        this.validator.showMessages(data.error.messages);
    }

    fatalError(data) {
        this.fireEvent('fail');
    }
}
